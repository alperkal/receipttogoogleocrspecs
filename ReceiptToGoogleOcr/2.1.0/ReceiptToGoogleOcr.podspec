#
# Be sure to run `pod lib lint ReceiptToGoogleOcr.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ReceiptToGoogleOcr'
  s.version          = '2.1.0'
  s.summary          = 'A library to scan receipt and return products'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
A library to scan receipt and return products
Calls Google Ocr and finds products in the returned text
                       DESC

  s.homepage         = 'https://bitbucket.org/alperkal/receipttogoogleocr.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Alper Sabri Kalaycioglu' => 'e1570670@ceng.metu.edu.tr' }
  s.source           = { :git => 'https://bitbucket.org/alperkal/receipttogoogleocr.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'ReceiptToGoogleOcr/Classes/**/*'
  
  s.dependency 'ObjectMapper', '~> 2.0.0'
  s.dependency 'Just', '~> 0.5.3'
end
